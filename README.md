# README #

Project inspired in video classes of "[Waibtec](http://www.thiagoporto.com.br/)" 

Example learning [15 of ytoutube](https://www.youtube.com/watch?v=pmjTsshPnok)

# Learnings #

* Módulos
* Callbacks
* Controllers
* Modules
* Routes
* Autoloader (express-loader)
* MongoDb (mongoose)
* Bootstrap (layout)
* CRUD

# LINKS OF LEARNINGS #

* [AULA 1 - Introdução](https://www.youtube.com/watch?v=TAZaYjk7klo&list=PLz6D6n_hNk4x255Eo4EbPsD9Kpsr2H7yc)

* [AULA 2 - Routes](https://www.youtube.com/watch?v=0ZQEBmnk6uU&list=PLz6D6n_hNk4x255Eo4EbPsD9Kpsr2H7yc&index=2)

* [AULA 3 - File system](https://www.youtube.com/watch?v=YjLvONgEles&index=3&list=PLz6D6n_hNk4x255Eo4EbPsD9Kpsr2H7yc)

* [AULA 4 - Change one page](https://www.youtube.com/watch?v=hizbR6ueX3k&list=PLz6D6n_hNk4x255Eo4EbPsD9Kpsr2H7yc&index=4)

* [AULA 5 - Modules](https://www.youtube.com/watch?v=MqJic4EWmCY&index=5&list=PLz6D6n_hNk4x255Eo4EbPsD9Kpsr2H7yc)

* [AULA 6 - NPM](https://www.youtube.com/watch?v=wKSaGBESlnE&list=PLz6D6n_hNk4x255Eo4EbPsD9Kpsr2H7yc&index=6)

* [AULA 7 - Package json](https://www.youtube.com/watch?v=t5518pHt2mU&index=7&list=PLz6D6n_hNk4x255Eo4EbPsD9Kpsr2H7yc)

* [AULA 8 - Frameworks](https://www.youtube.com/watch?v=MO9G6O0CLow&index=8&list=PLz6D6n_hNk4x255Eo4EbPsD9Kpsr2H7yc)

* [AULA 9 - Struct of aplication](https://www.youtube.com/watch?v=MO9G6O0CLow&index=8&list=PLz6D6n_hNk4x255Eo4EbPsD9Kpsr2H7yc)

* [AULA 10 - Create routes and controllers](https://www.youtube.com/watch?v=NHKjmY67xKc&list=PLz6D6n_hNk4x255Eo4EbPsD9Kpsr2H7yc&index=10)

* [AULA 11 - Add bootstrap layout of project](https://www.youtube.com/watch?v=V5hQ9cEsaI8&index=11&list=PLz6D6n_hNk4x255Eo4EbPsD9Kpsr2H7yc)

* [AULA 12 - Module Nodemon (change updates in code)](https://www.youtube.com/watch?v=IgXr18Q4g14&list=PLz6D6n_hNk4x255Eo4EbPsD9Kpsr2H7yc&index=12)

* [AULA 13 - Mongoose - mongoDb](https://www.youtube.com/watch?v=Bmt-cBdRE10&index=13&list=PLz6D6n_hNk4x255Eo4EbPsD9Kpsr2H7yc)

* [AULA 14 - Testing connection in mongoDb](https://www.youtube.com/watch?v=hjZCTwt7ehI&list=PLz6D6n_hNk4x255Eo4EbPsD9Kpsr2H7yc&index=14)

* [AULA 15 - Create User #1](https://www.youtube.com/watch?v=pmjTsshPnok&index=15&list=PLz6D6n_hNk4x255Eo4EbPsD9Kpsr2H7yc)

