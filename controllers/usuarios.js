module.exports = function(app){

	var Usuario = app.models.usuarios;

	var UsuarioController = {

		index: function(req, res){
			res.render('usuarios/index');
		},
		cadastro: function(req, res){

			var model = new Usuario({
				nome: 'JOJO',
				login: 'jojo',
				senha: 'jojo'
			});

			model.save(function(err, data){
				if(err){
					console.log(err);
				}else{
					res.json(data);
				}
			});
		}
	}

	return UsuarioController;
}
